package com.example.backend.dto;

import com.example.backend.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserDto {
    private String username;
    private String firstName;
    private String lastName;

    public UserDto(User user) {
        username = user.getUsername();
        firstName = user.getFirstName();
        lastName = user.getLastName();
    }
}

package com.example.backend.dto;

import com.example.backend.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserSettingsDto {
    private String username;
    private boolean twoFAEnabled;
    private String tgId;

    public UserSettingsDto(User user) {
        username = user.getUsername();
        twoFAEnabled = user.isTwoFAEnabled();
        tgId = user.getTgId();
    }
}

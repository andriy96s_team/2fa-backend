package com.example.backend.service;

import com.example.backend.dto.UserSettingsDto;
import com.example.backend.dto.UserSignupDto;
import com.example.backend.entity.User;
import com.example.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;

@Service
@PropertySource("classpath:application.properties")
public class UserService {

    private UserRepository userRepository;
    @Value("${tg.service.url}")
    private String tgUrl;


    @Autowired
    public void setUserRepository(UserRepository repository) {
        this.userRepository = repository;
    }


    @Transactional
    public User getUserById(long id) {
        return userRepository.findById(id).orElse(new User());
    }

    @Transactional
    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }


    @Transactional
    public User saveUser(UserSignupDto userSignupDto) {
        User user = new User();
        user.setFirstName(userSignupDto.getFirstName());
        user.setLastName(userSignupDto.getLastName());
        user.setUsername(userSignupDto.getUsername());
        user.setPassword(userSignupDto.getPassword());

        return userRepository.save(user);
    }

    @Transactional
    public void set2FA(UserSettingsDto dto) {
        User user = userRepository.findByUsername(dto.getUsername());
        user.setTwoFAEnabled(dto.isTwoFAEnabled());
        user.setTgId(dto.isTwoFAEnabled() ? dto.getTgId() : "");
        userRepository.save(user);
    }

    public void sendTgCode(String username) {
        User user = userRepository.findByUsername(username);

        final String url = tgUrl + "/user/sendcode/" + user.getTgId();

        sendGetRequest(url);
    }

    public boolean verifyCode(String username, String code) {
        User user = userRepository.findByUsername(username);

        final String url = tgUrl + "/user/verify/" + user.getTgId() + "/" + code;
        ResponseEntity responseEntity = sendGetRequest(url);
        return responseEntity.getStatusCode().is2xxSuccessful();
    }

    private ResponseEntity sendGetRequest(String url) {
        RestTemplate restTemplate = new RestTemplate();

//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
        ResponseEntity responseEntity = null;
        try {
            responseEntity = restTemplate.getForEntity(url, String.class);
        } catch (HttpClientErrorException e) {
            if (e instanceof HttpClientErrorException.BadRequest) {
                responseEntity = new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        }
        return responseEntity;
    }
}

package com.example.backend.controller;

import com.example.backend.service.UserService;
import com.example.backend.dto.UserDto;
import com.example.backend.dto.UserLoginDto;
import com.example.backend.dto.UserSettingsDto;
import com.example.backend.dto.UserSignupDto;
import com.example.backend.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

//@CrossOrigin(origins = "http://localhost:3000, https://two-factor-auth-demo.herokuapp.com")
@CrossOrigin
@RestController
@RequestMapping("/api/user")
public class UserController {


    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<User> getUser() {
        User user = userService.getUserById(1);
        System.out.println(user);
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }

    @PostMapping("/signup")
    public ResponseEntity<UserDto> signup(@RequestBody UserSignupDto signupDto) {
        User user = userService.saveUser(signupDto);
        return new ResponseEntity<>(new UserDto(user), HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody UserLoginDto loginDto) {
        User user = userService.getUserByUsername(loginDto.getUsername());
        if (user != null && loginDto.getPassword().equals(user.getPassword())) {
            if(user.isTwoFAEnabled()) {
                userService.sendTgCode(loginDto.getUsername());
                return new ResponseEntity<>("Need code", HttpStatus.PARTIAL_CONTENT);
            }
            else {
                return new ResponseEntity<UserDto>(new UserDto(user), HttpStatus.OK);
            }
        } else {
            return new ResponseEntity<>("Incorrect username or password", HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/change2FA")
    public ResponseEntity change2FA(@RequestBody UserSettingsDto userDto) {
        System.out.println(userDto);
        try {
            userService.set2FA(userDto);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

    @GetMapping("/sendcode/{username}")
    public ResponseEntity sendTgCode(@PathVariable String username) {
        userService.sendTgCode(username);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping("/verifyCode/{username}/{code}")
    public ResponseEntity verifyCode(@PathVariable String username, @PathVariable String code) {
        boolean isCorrect = userService.verifyCode(username, code);
        if(isCorrect) {
            User user = userService.getUserByUsername(username);
            return new ResponseEntity<>(new UserDto(user), HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/getSettings/{username}")
    public ResponseEntity<UserSettingsDto> getSettings(@PathVariable String username) {
        User user = userService.getUserByUsername(username);
        if(user!=null) {
            return new ResponseEntity<>(new UserSettingsDto(user), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

}
